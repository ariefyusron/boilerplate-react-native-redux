React Native Boilerplate - January 2019
===========================================

## Features

* [Axios](https://github.com/axios/axios)
* [NativeBase](https://nativebase.io/)
* [React Navigation](https://reactnavigation.org/)
* [Redux](http://redux.js.org/)
* [Redux Form](https://redux-form.com/8.1.0/)
* [Redux Logger](https://github.com/LogRocket/redux-logger)
* [Redux Promise Middleware](https://github.com/pburtchaell/redux-promise-middleware)


## Getting Started

1. Clone this repo, `git clone https://github.com/ariefyusron/boilerplate-react-native-redux.git <your project name>`
2. Go to project's root directory, `cd <your project name>`
3. Remove `.git` folder,  `rm -rf .git`
4. Run `yarn` or `npm install` to install dependencies
5. Start the packager with `npm start`
6. Connect a mobile device to your development machine
7. Run the test application:
  * On Android:
    * Run `react-native run-android`
  * On iOS:
    * Run `react-native run-ios`
8. Enjoy!!!